import networkx as net
import pandas as pd
import matplotlib.pyplot as plt
import statistics as stat
import numpy as np

# init
layout = net.spring_layout
timestamp = []
nodes = []
edges = []
degree_value = []
# load data
data = open("data/names.txt", "r")
# manage graph elements
for row in data:
	row = row.replace('\n', '')
	t, no, ne = row.split(" ")
	timestamp.append(t)
	nodes.append(no)
	edges.append([no, ne])
data.close()		
print "\nfile loaded."

#***************************************************************#
#UNDIRECTED GRAPH
#***************************************************************#
# create the graph
graph = net.Graph()
graph.add_edges_from(edges)
graph.add_nodes_from(nodes)
#pos = layout(graph)

print "undirected graph created."
print "====================="

#***************************************************************#
# Q1: number of nodes and edges
n_nodes = graph.number_of_nodes()
n_edges = graph.number_of_edges()

print "nodes: "+str(n_nodes)
print "edges: "+str(n_edges)
print "====================="

#***************************************************************#
# Q2: degree CCDF and mean degree
degree = sorted(graph.degree, key=lambda x: x[1], reverse=True)
for item in degree:
	degree_value.append(item[1])
# save CCDF
with open("output/CCDF.dat", "a") as file:
	for item in degree_value:
		file.write(str(item)+"\n")

mean_degree = stat.mean(degree_value)
print "mean degree: "+str("%.3f"%mean_degree)
print "====================="

#***************************************************************#
# Q3: average clustering coefficient
clst = net.average_clustering(graph)
print "clustering: "+str("%.3f"%clst)
print "====================="

#***************************************************************#
# Q4: giant component
sqr_degree=[]
for item in degree:
	sqr_degree.append(item[1]**2)
giant_condition = stat.mean(sqr_degree) - 2*mean_degree

# number of connected components
n_connected = net.number_connected_components(graph)
print "connected components: "+str(n_connected)

# check giant components
print "giant component condition:"
if giant_condition >= 0:
	print "\tsatisfied: "+str("%.3f"%giant_condition)+" > 0"
	
	# find giant component
	giant = max(net.connected_component_subgraphs(graph,  copy=False), key = len)
	
	print "giant component:"
	print "\tsize: "+str(len(giant))
		
	"""
	#plot graph w/ giant component
	plt.figure()
	net.draw(graph, 
			pos, 
			alpha=0.8, 
			node_color='r', 
			node_size=8,
	)
	net.draw_networkx_edges(giant, 
							pos,
							edge_color='g',
							width=2.0,
							with_labels=False,
	)
	net.draw_networkx_nodes(giant, 
							pos,
							with_labels=True,
							node_color='g',
							node_size=8
	)
	plt.draw()
	plt.savefig("output/und_n+g.png")
	
	#plot graph w/out giant component
	plt.figure()
	net.draw(graph, 
			pos, 
			alpha=0.8, 
			node_color='r', 
			node_size=8,
	)
	plt.draw()
	plt.savefig("output/und_n.png")
	
	#plot giant component
	plt.figure()
	net.draw(giant, 
			pos, 
			alpha=0.8, 
			node_color='g', 
			node_size=15,
	)
	plt.draw()
	plt.savefig("output/und_g.png")
	"""
else:
	print "\tnot satisfied: "+str("%.3f"%giant_condition)+" < 0"


#***************************************************************#
#DIRECTED GRAPH
#***************************************************************#
# create the graph
graph = net.DiGraph()
graph.add_edges_from(edges)
graph.add_nodes_from(nodes)
#pos = layout(graph)

print "\ndirected graph created."
print "====================="

#***************************************************************#
# Q5: strongly-connected components
# number of strongly connected components
n_strongly_connected = net.number_strongly_connected_components(graph)
giant_strong = max(net.strongly_connected_component_subgraphs(graph), key = len)
e = net.eccentricity(giant_strong)
d_strong = net.diameter(giant_strong, e=e)
r_strong = net.radius(giant_strong, e=e)
c_strong = net.center(giant_strong, e=e)
print "strongly connected: "+str(n_strongly_connected)
print "biggest strongly connected:"
print "\tsize: "+str(len(giant_strong))
print "\tcenter: "+str(c_strong)
print "\tdiameter: "+str(d_strong)
print "\tradius: "+str(r_strong)

#***************************************************************#
# Q6: weakly-connected components
# number of weakly connected components
n_weakly_connected = net.number_weakly_connected_components(graph)
giant_weak = max(net.weakly_connected_component_subgraphs(graph), key = len)
#d_weak = net.diameter(giant_weak) # ERROR: not strongly-connected
#r_weak = net.radius(giant_weak) # ERROR: not strongly-connected
#c_weak = net.center(giant_weak) # ERROR: not strongly-connected
print "weakly connected: "+str(n_weakly_connected)
print "biggest weakly connected:"+str(len(giant_weak))
print "\tsize: "+str(len(giant_weak))

"""
# plot graph w/out components
plt.figure()
net.draw(graph, 
		pos, 
		alpha=0.8, 
		node_color='r', 
		node_size=8,
)
plt.draw()
plt.savefig("output/dir_n.png")

# plot weakly-connected components
plt.figure()
net.draw(giant_weak, 
		pos, 
		alpha=0.8, 
		node_color='b', 
		node_size=15,
)
plt.draw()
plt.savefig("output/dir_w.png")

# plot strongly-connected components
plt.figure()
net.draw(giant_strong, 
		pos, 
		alpha=0.8, 
		node_color='g', 
		node_size=15,
)
plt.draw()
plt.savefig("output/dir_s.png")

# plot graph w/ components
plt.figure()
net.draw(graph, 
		pos, 
		alpha=0.8, 
		node_color='r', 
		node_size=8,
)
net.draw_networkx_edges(giant_weak, 
						pos,
						with_labels=False,
						edge_color='b',
						width=2.0,
)
net.draw_networkx_nodes(giant_weak,
						pos,
						with_labels=False,
						node_color='b',
						node_size=8
)
net.draw_networkx_edges(giant_strong, 
						pos,
						with_labels=False,
						edge_color='g',
						width=2.0
)
net.draw_networkx_nodes(giant_strong,
						pos,
						with_labels=False,
						node_color='g',
						node_size=8
)
plt.draw()
plt.savefig("output/dir_n+w+s.png")
"""
