#!/usr/bin/python3
import simpy
import networkx as net
import pandas as pd
import random
import sys


random.seed(7)


class Graph():
	"""Load a set of nodes and link from the 'name.txt' file and create an undirected
	graph. The nodes properties are used to simulate the spreading of a message between
	the nodes of the graph.
	
	Attributes
	----------
		node : list
			list of the graph nodes
		link : list
			list of the graph links
		graph : networkx.classes.graph.Graph
			undirected graph built from the nodes and links lists
			
	Methods
	-------
		create()
			load the nodes and their relationships from a file and create the undirected 
			graph
			
	"""
	def __init__(self):
		# graph init
		layout = net.spring_layout
		self.node = []
		self.link = []
		self.graph = net.Graph()
		
		# create graph
		self.create()
		print "Graph created."


	def create(self):
		"""Load the nodes and their relationships from the 'data/names.txt' file and create 
		the undirected graph by using the file entries as nodes.
		
		Attributes
		----------
			data : file
				.txt file storing the nodes names and their relationships
				
		"""
		# load data
		self.data = open("data/names.txt", "r")
		cnt = 0

		# manage graph elements
		for row in self.data:
			row = row.replace('\n', '')
			t, no, ne = row.split(" ")
			if cnt==LIM:
				break
			self.node.append(no)
			self.link.append([no, ne])
			cnt+=1
		
		self.data.close()		
		
		print "\nFile loaded."
		
		# add graph nodes/edges
		self.graph.add_edges_from(self.link)
		self.graph.add_nodes_from(self.node)



class Infection():
	"""By considering the graph as a social network, the infection can be seen as a 
	particular message which is forwarded through the graph's node. 
	The infection is performed by observing the neighbors of not infected nodes. If the 
	number of infected neighbors of a 'healthy' node is greater than the MININF percentage, 
	than the node is infected.
	After a new infection the graph status is updated.
		
	Parameters
	----------
		graph : networkx.classes.graph.Graph
			undirected graph built from the nodes and links lists
		env : simpy.core.Environments
			SimPy simulation environment
		
	Attributes
	----------
		g : networkx.classes.graph.Graph
			undirected graph built from the nodes and links lists
		env : simpy.core.Environments
			SimPy simulation environment
		data : dict
			variable to be converted in a pandas DataFrame to plot the results
		cnt : int
			simulation steps counter
		active : list
			list of the first infected nodes. It is randomly generated
	
	Methods
	-------
		survivalFunction()
			determine the ratio between the nodes that are not infected and the total number
			of nodes at each simulation step
		update(new, tot, survival)
			after each simulation step, the number of new infected nodes, of total infected
			nodes and the survival nodes ratio is stored in a file
		run()
			at each simulation step a certain number of nodes is infected by observing its
			neighbors
			
	"""
	def __init__(self, graph, env):
		
		self.g = graph
		self.env = env
		self.data = {
			'new.infected':[],
			'tot.infected':[],
			'survival':[],
			'range':[]
		}
		self.cnt = 0
		
		# first infection
		self.active=random.sample(self.g.graph.node, FSTSET)
		net.set_node_attributes(self.g.graph, 0, 'active')
		
		for i in self.active:
			self.g.graph.nodes[i]['active'] = 1
		
		env.process(self.run())
	
		
	def survivalFunction(self):
		"""Determine the ratio between the nodes that are not infected and the total number
		of nodes at each simulation step.
		
		Returns
		-------
			surv : float
				ratio between the nodes that are not infected and the total number of nodes
				at each simulation step
				
		"""
		n_nodes = self.g.graph.number_of_nodes()
		n_active = len(self.active)
		n_inactive = float(n_nodes-n_active)
		
		surv = n_inactive/n_nodes
		
		return surv

	
	def update(self, new, tot, survival):
		"""After each simulation step, the number of new infected nodes, of total infected
		nodes and the survival nodes ratio is stored in a file in the 'output' folder.
		
		Attributes
		----------
			new : int
				number of new node infected at each simulation step
			tot : int
				number of total node infected at each simulation step
			survival : float
				ratio between the nodes that are not infected and the total number of nodes
				at each simulation step
				
		"""
		self.data['new.infected'].append(new)
		self.data['tot.infected'].append(tot)
		self.data['survival'].append(survival)
		self.data['range'].append(self.cnt)
		self.cnt+=1
		
		df = pd.DataFrame(self.data)
		df.to_csv("output/infection_{}_{}.csv".format(PERC, MININF))		
	
	
	def run(self):
		"""The infection is performed by observing the neighbors of not infected nodes. If 
		the number of infected neighbors of a 'healthy' node is greater than the MININF
		percentage, than the node is infected.
		After a new infection the graph status is updated.
		
		Yields
		------
			simpy.events.Timeout
				after a number of seconds defined in DELTA_T
				
		"""
		print "Number of nodes: {}".format(len(self.g.graph.nodes))
		print "First infection:", len(self.active), "nodes infected."
		print "Survivals:%.3f "%self.survivalFunction()
		
		self.update(0, len(self.active), self.survivalFunction())
		
		while True:
			activating = []			
			prev_infected = len(self.active)
			
			for node in self.g.graph.nodes:
				if node not in self.active:
					# list the node's neighbors
					neighbors = list(self.g.graph.neighbors(node))
					actv_cnt = 0
					for neigh in neighbors:
						# check if the neighbors of the node are infected
						if neigh in self.active:
							actv_cnt += 1
					
					# define the minimum number of infected neighbors to infect the node
					to_activate = int(len(neighbors)*MININF/100)
					
					# if the number of infected neighbors are greater than the threshold,
					# infect it
					if actv_cnt > to_activate:
						activating.append(node)
			
			# update the infected nodes
			for node in activating:		
				self.g.graph.nodes[node]['active'] == 1
				self.active.append(node)
			
			new_infected = len(self.active) - prev_infected
			self.update(new_infected, len(self.active), self.survivalFunction())		
			print "---------------------"
			print "New nodes infected: {}".format(new_infected)
			print "Total infected nodes: {}".format(len(self.active))
			print "Survivals: {}".format(self.survivalFunction())
						
			yield env.timeout(DELTA_T)


#======= MAIN =======

if len(sys.argv)!=3:
	print "usage: python lab1.2.py <m> <p>"
	print """
m - Mininum percentage of infected neighbors to spread the infection
p - Percentage of the first nodes infection
	"""
	exit()
else:
	# constants
	LIM		= 100000

	MININF	= float(sys.argv[1]) # minimum percentage of infected neighbors
	PERC	= float(sys.argv[2]) # first infection percentage

	FSTSET 	= int(PERC*LIM/100) 

	START	= 0 # sterting time
	DELTA_T	= 5 # interval time
	SIMTIME	= 50 # simulation time
	
	g = Graph()
	env = simpy.Environment(initial_time=START)
	Infection(g, env)
	env.run(until=SIMTIME)
	
	print "\n\nSimulation Ended."
